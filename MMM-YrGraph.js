Module.register('MMM-YrGraph', {
    defaults: {
        yrApiUrl: "https://www.yr.no/api/v0/locations/id/%s/forecast",
        updateInterval: 10
    },

    getScripts: function() {
        return [
            'printf.js',
            'graph.js',
            'https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js'
        ];
    },

    getStyles: function() {
        return ['mmm-yrgraph.css'];
    },

    start: function() {
        this.list = null;
        this.loaded = false;

        this.sendSocketNotification('GET_YR_FORECAST', {
            forecastUrl: printf(printf('%s', this.config.yrApiUrl), this.config.locationId),
            updateInterval: this.config.updateInterval
        });
    },

    socketNotificationReceived: function(notification, payload) {
        if (notification === 'YR_FORECAST_DATA') {
            this.processForecast(payload.forecast);
            this.updated = payload.timestamp;
            this.updateDom(1000);
            console.log('YR_FORECAST_DATA received');
        }
    },


    getDom: function() {
        var wrapper = document.createElement('div');

        if (!this.loaded) {
            wrapper.innerHTML = this.translate('loading');
            wrapper.className = 'dimmed light small';
            return wrapper;
        }

        var canvas = document.createElement('canvas');
        canvas.id = 'canvas';
        canvas.width = 600;
        canvas.height = 200;
        var ctx = canvas.getContext('2d');
        createChart(ctx, this.graph_data);
        wrapper.appendChild(canvas);
        return wrapper;
    },

    processForecast: function(forecast) {
        if (forecast.shortIntervals) {
            let data = {
                start: [],
                temperatures: [],
                precipitation: [],
                precipitation_min: [],
                precipitation_max: []
            };

            let startTime = new Date(forecast.shortIntervals[0].start);
            console.log("Start time:", startTime.getHours());
            
            if (startTime.getHours() % 2) {
                data.start.push((startTime.getHours() + 23) % 24);
                data.temperatures.push(null);
                data.precipitation.push(null);
                data.precipitation_min.push(null);
                data.precipitation_max.push(null);
            }

            console.log(data.start, data.temperatures, data.precipitation, data.precipitation_min, data.precipitation_max);

            forecast.shortIntervals.forEach(i => {
                var date = new Date(i.start);
                data.start.push(date.getHours());
                data.temperatures.push(i.temperature.value);
                data.precipitation.push(i.precipitation.value);
                data.precipitation_min.push(i.precipitation.min);
                data.precipitation_max.push(i.precipitation.max);
            });

            data.temperature_max = Math.ceil(Math.max(...data.temperatures) + 0.5);
            data.temperature_min = Math.ceil(Math.min(...data.temperatures) - 0.5);

            this.graph_data = data;

            this.loaded = true;
        }
    }
});