# MagicMirror² Module: YrGraph

This is module for [MagicMirror²](https://github.com/MichMich/MagicMirror), which displays data from [Yr](https://www.yr.no/nb/) in graph format.

![Screenshot](./screenshot.png)

## How to install

Remote into your Magic Mirror box using a terminal software and go to the modules folder:

    cd ~/MagicMirror/modules

Clone the repository:

	git clone https://somewhere/something

Add the module to the modules array in the config/config.js file by adding the following section. You can change this configuration later when you see this works:

	{
      module: 'MMM-YrGraph',
      position: 'bottom_right',
      config: {
        locationId: '1-73738',
      }
    },

## Configuration options

|Option|Comment|Default|
|------|-------|-------|
|locationId|The unique Id found in the Url of any location on [Yr](https://www.yr.no/nb/liste/dag/1-73738/Norge/Oslo/Oslo/Blindern) I.e. Blindern (Oslo)|1-73738|
