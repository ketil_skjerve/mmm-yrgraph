var NodeHelper = require('node_helper');
var request = require('request');
var moment = require('moment');

module.exports = NodeHelper.create({
	start: function() {
		console.log('Starting node helper for: ' + this.name);
        this.forecastUrl = '';
	},

	socketNotificationReceived: function(notification, payload) {
		var self = this;
        if (notification === 'GET_YR_FORECAST') {
            self.forecastUrl = payload.forecastUrl;
            this.getForecast();

            if (!self.timer) {
                self.timer = setInterval(function() { self.getForecast(); }, payload.updateInterval*60*1000);
            }
        }
	},

	getForecast: function() {
		var self = this;
        var locationData = {};

        request({url: self.forecastUrl, method: 'GET'}, function(error, response, message) {
            if (!error && (response.statusCode == 200 || response.statusCode == 304)) {
                locationData.forecast = JSON.parse(message);
                locationData.timestamp = moment().format();
                self.sendSocketNotification('YR_FORECAST_DATA', locationData);
            }
        });
	},
});
