
function createChart(context, data) {
    let chartColors = {
        line: 		'#fff',
        rain:		'#ddd',
        rain_max:	'#777',
        gridLines: 	'#444',
        zeroLines:  '#fff',
        labels:		'#aaa',
    };


    let hourGridLines = data.start.map(h => h === 0 ? chartColors.zeroLines : chartColors.gridLines);   

    let config = {
        type: 'bar',
        data: {
            labels: data.start,
            datasets: [{
                type: 'line',
                yAxisID: 'TempYAxis',
                data: data.temperatures,
                borderColor: chartColors.line,
                backgroundColor: 'rgba(0, 0, 0, 0)',
                borderWidth: 2,
                fill: false
            },{
                type: 'bar',
                yAxisID: 'PercipYAxis',
                data: data.precipitation,
                backgroundColor: chartColors.rain,
                borderWidth: 0
            },{
                type: 'bar',
                yAxisID: 'PercipYAxis',
                data: data.precipitation_max,
                backgroundColor: chartColors.rain_max,
                borderWidth: 0
            }]
        },
        options: {
            responsive: false,
            elements: {
                point:{
                    radius: 0
                }
            },
            title: { display: false },
            legend: { display: false },
            tooltips: {
                mode: 'index'
            },
            scales: {
                xAxes: [{
                    display: true,
                    stacked: true,
                    ticks: {
                        fontColor: chartColors.labels,
                        autoSkip: true,
                        maxRotation: 0,
                        padding: 5
                    },
                    gridLines: {
                        color: hourGridLines,
                        zeroLineColor: chartColors.zeroLines,
                        zeroLineWidth: 1,
                        offsetGridLines: false,
                    }
                }],
                yAxes: [{
                    id: 'TempYAxis',
                    display: true,
                    ticks: {
                        suggestedMin: data.temperature_min,
                        suggestedMax: data.temperature_max,
                        stepSize: 1,
                        callback: function(value, index, values) {
                            return value + '°';
                        },
                        fontColor: chartColors.labels
                    },
                    gridLines: {
                        color: chartColors.gridLines,
                        zeroLineColor: chartColors.zeroLines,
                        zeroLineWidth: 1,
                        drawBorder: false
                    }
                },{
                    id: 'PercipYAxis',
                    display: false,
                    ticks: {
                        max: 10,
                        min: 0
                    }
                }]
            },
            animation: {
                easing: 'easeOutQuart',
                duration: 500
            }
        }
    };

    return new Chart(context, config);
}

